const async = require('async');
let log = require('@dosarrest/loopback-component-logger')('mod-admin-acls/boot');
class Boot {
  constructor(app, done) {
    let me = this;
    me.app = app;
    me.done = done;
    me.init();
  }
  init() {
    let me = this;
    let app = me.app;
    let done = me.done;
    let registry = app.registry;
    let ACL = registry.getModelByType('ACL');
    let AdminNavigation = registry.getModelByType('AdminNavigation');
    async.series({
      AdminNavigation: (cb) => {
        AdminNavigation.find({where: {routeId: 'acls', perspective: 'admin'}}).then(navItem => {
          if (navItem.length === 0) {
            AdminNavigation.create({
              text: 'mod-admin-acls:ACLS',
              perspective: 'admin',
              iconCls: 'x-fa fa-lock',
              // rowCls: 'nav-tree-badge nav-tree-badge-new',
              viewType: 'acls',
              routeId: 'acls',
              leaf: true,
            }).then(newNavItem => {
              log.info(newNavItem);
              cb(null, true);
            }).catch(err => {
              log.error(err);
              cb(null, false);
            });
          } else {
            cb(null, true);
          }
        }).catch(err => {
          log.error(err);
          cb(null, true);
        });
      },
      EveryoneACL(cb) {
        ACL.findOrCreate({
          model: ACL.definition.name,
          accessType: '*',
          principalType: 'ROLE',
          principalId: '$everyone',
          permission: 'DENY',
          property: '*'
        }).then((acl) => {
          cb(null, true);
        }).catch((err) => {
          log.error(err);
          cb(null, true)
        });
      },
      AdminACL(cb) {
        ACL.findOrCreate({
          model: ACL.definition.name,
          accessType: '*',
          principalType: 'ROLE',
          principalId: 'Administrator',
          permission: 'ALLOW',
          property: '*'
        }).then((acl) => {
          cb(null, true);
        }).catch((err) => {
          log.error(err);
          cb(null, true)
        });
      }
    }, (err, results) => {
      done(null, true);
    });

  }
}
module.exports = Boot;
