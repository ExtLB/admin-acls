Ext.define('Client.view.admin.acls.ViewModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.admin.acls',
  requires: [
    'Ext.data.field.Date',
    'Ext.data.proxy.Rest',
    'Ext.data.reader.Json'
  ],
	data: {
		name: 'Client',
    role: {},
    extraACLParams: {
		  filter: '{"where": {}, "include": [{"relation": "user", "scope": {"fields": ["username","email"]} }] }'
    }
	},
  stores: {
	  aclStore: {
	    autoLoad: true,
      fields: [{
        name: 'id',
        persist: false
      }, {
        name: 'model'
      }, {
        name: 'property'
      }, {
        name: 'accessType'
      }, {
        name: 'permission'
      }, {
        name: 'principalType'
      }, {
        name: 'principalId'
      }],
      proxy: {
        type: 'rest',
        url: '{apiUrl}/ACLs',
        // extraParams: '{extraACLParams}',
        actionMethods: {
          create:"POST",
          read:"GET",
          update:"PATCH",
          destroy:"DELETE"
        },
        reader: {
          type: 'json'
        }
      }
    }
  }
});
