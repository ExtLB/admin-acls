Ext.define('Client.view.admin.acls.ViewController', {
	extend: 'Ext.app.ViewController',
	alias: 'controller.admin.acls',

  onACLDblTap: function(grid, location, eOpts) {
    let editor = Ext.create('Client.view.admin.acl.Editor', {
	    handler: function(user) {
	      user.save({
          success: () => {
            Ext.toast(i18next.t('saa:SAVE_ACL_SUCCESS'));
          },
          failure: () => {
            Ext.toast(i18next.t('saa:SAVE_ACL_FAILURE'));
          }
        });
      },
	    acl: location.record
    });
	  editor.show();
  },
  onEditACL: function(owner, eOpts) {
    let editor = Ext.create('Client.view.admin.acl.Editor', {
	    handler: function(user) {
	      user.save({
          success: () => {
            Ext.toast(i18next.t('saa:SAVE_ACL_SUCCESS'));
          },
          failure: () => {
            Ext.toast(i18next.t('saa:SAVE_ACL_FAILURE'));
          }
        });
      },
	    acl: eOpts.record
    });
	  editor.show();
  },
  onRemoveACL: function(owner, eOpts) {
    let rec = eOpts.record;
    if (rec.get('id') === Client.app.getController('Authentication').user.id) {
     Ext.Msg.alert(i18next.t('saacl:ERROR'), i18next.t('saacl:ERROR_REMOVE_ACL_MINE'));
    } else {
      let template = new Ext.XTemplate(i18next.t('saacl:CONFIRM_REMOVE_ACL_MESSAGE'));
      Ext.Msg.confirm(i18next.t('saacl:CONFIRM_REMOVE_ACL_TITLE'), template.apply({rec: rec}), (btn) => {
        if (btn === 'yes') {
          rec.erase({
            callback: (record, operation, success) => {
              if (success !== true) {
                Ext.toast(i18next.t('saacl:CONFIRM_REMOVE_ACL_ERROR'));
              } else {
                Ext.toast(i18next.t('saacl:CONFIRM_REMOVE_ACL_SUCCESS'));
              }
            }
          });
        }
      });
    }
  },

  onAddACL: function() {
	  let me = this;
	  let view = me.getView();
	  let vm = view.getViewModel();
	  let store = vm.getStore('roleStore');
	  let editor = Ext.create('Client.view.admin.acl.Editor', {
	    handler: function(role) {
	      store.add(role);
	      store.sync({
          success: () => {
            Ext.toast(i18next.t('saa:ADD_ACCOUNT_SUCCESS'));
          },
          failure: () => {
            Ext.toast(i18next.t('saa:ADD_ACCOUNT_FAILURE'));
          }
        });
      }
    });
	  editor.show();
  },

  onRoleUserCancel: function(btn) {
    let me = this;
    let list = me.lookup('roleList');
    list.getSelectable().getSelectedRecords()[0].reject();
  },
  onRoleUserSave: function(btn) {
	  let me = this;
	  let list = me.lookup('roleList');
    let form = me.lookup('roleUserForm');
    if (form.validate()) {
      let record = list.getSelectable().getSelectedRecords()[0];
      btn.disable();
      record.save({
        success: () => {
          btn.enable();
        },
        failure: () => {
          Ext.toast(i18next.t('saa:ERROR_ACCOUNT_USER_SAVE_FAILURE'));
          btn.enable();
        }
      });
    }
  },
  onRoleUserGridSelect: function(grid, selected, eOpts) {
    let me = this;
    let panel = me.lookup('roleUserPanel');
    panel.setMasked(false);
  },
  onRoleUserGridDeselect: function(grid, selected, eOpts) {
	  let me = this;
    let panel = me.lookup('roleUserPanel');
    panel.setMasked({
      xtype: 'loadmask',
      message: i18next.t("saa:SELECT_ACCOUNT_USER")
    });
    panel.reset();
  },

	onItemSelected: function (sender, record) {
		Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
	},

	onConfirm: function (choice) {
		if (choice === 'yes') {
			//
		}
	},

  onRefreshACLs: function() {
	  let me = this;
	  let view = me.getView();
	  let vm = view.getViewModel();
	  vm.getStore('aclStore').load();
  },

  onACLGridInitialize: function() {
    let me = this;
  }
});
