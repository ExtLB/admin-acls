Ext.define('Client.view.admin.acls.View', {
	// extend: 'Ext.grid.Grid',
  extend: 'Ext.Container',
	xtype: 'admin.acls',
	cls: 'admin-acls',
	requires: [
	  'Ext.XTemplate',
    'Ext.layout.Fit',
    'Ext.grid.Grid',
    'Ext.field.Text',
    'Ext.field.Select',
    'Ext.Toolbar',
    'Ext.Button',
    'Ext.Spacer',
    'Ext.LoadMask',
    'Ext.data.validator.Email'
  ],
	controller: {type: 'admin.acls'},
	viewModel: {type: 'admin.acls'},
  perspectives: ['admin'],

  layout: 'fit',
  items: [{
    xtype: 'grid',
    flex: 1,
    reference: 'roleUserGrid',
    bind: {
      store: '{aclStore}'
    },
    listeners: {
      initialize: 'onACLGridInitialize',
      childdoubletap: 'onACLDblTap'
    },
    items: [{
      xtype: 'toolbar',
      docked: 'top',
      items: [{
        xtype: 'button',
        ui: 'round raised',
        handler: 'onRefreshACLs',
        iconCls: 'x-fa fa-sync'
      }, {
        xtype: 'spacer'
      }, {
        xtype: 'button',
        ui: 'round raised',
        handler: 'onAddACL',
        iconCls: 'x-fa fa-plus'
      }]
    }],
    columns: [{
      bind: {
        text: '{"mod-admin-acls:ACL_COLUMN.MODEL":translate}',
      },
      dataIndex: 'model',
      width: 100,
      cell: {userCls: 'bold'}
    }, {
      bind: {
        text: '{"mod-admin-acls:ACL_COLUMN.PROPERTY":translate}',
      },
      dataIndex: 'property',
      flex: 1,
      cell: {userCls: 'bold'}
    }, {
      bind: {
        text: '{"mod-admin-acls:ACL_COLUMN.ACCESSTYPE":translate}',
      },
      dataIndex: 'accessType',
      width: 110,
      cell: {userCls: 'bold'}
    }, {
      bind: {
        text: '{"mod-admin-acls:ACL_COLUMN.PERMISSION":translate}',
      },
      dataIndex: 'permission',
      width: 100,
      cell: {userCls: 'bold'}
    }, {
      bind: {
        text: '{"mod-admin-acls:ACL_COLUMN.PRINCIPALTYPE":translate}',
      },
      dataIndex: 'principalType',
      width: 120,
      cell: {userCls: 'bold'}
    }, {
      bind: {
        text: '{"mod-admin-acls:ACL_COLUMN.PRINCIPALID":translate}',
      },
      dataIndex: 'principalId',
      flex: 1,
      cell: {
        tools: {
          editACL: {
            handler: 'onEditACL',
            zone: 'end',
            bind: {
              tooltip: '{"mod-admin-acls:EDIT_ACL":translate}'
            },
            iconCls: 'x-fa fa-edit'
          },
          removeACL: {
            handler: 'onRemoveACL',
            zone: 'end',
            bind: {
              tooltip: '{"mod-admin-acls:REMOVE_ACL":translate}'
            },
            iconCls: 'x-fa fa-times'
          }
        }
      }
    }]
  }]
  // bind: {
  //   store: '{accountStore}',
  // },
  // selectable: { mode: 'single' },
  // listeners: {
		// select: 'onItemSelected'
  // },

});
