Ext.define('Client.view.admin.acl.Editor', {
  extend: 'Ext.Dialog',
  xtype: 'admin.acl.editor',
  requires: [
    'Ext.Toolbar',
    'Ext.Button',
    'Ext.Spacer',
    'Ext.form.Panel',
    'Ext.field.Select',
    'Ext.field.TextArea',
    'Ext.dataview.List'
  ],
  viewModel: {type: 'admin.acl.editor'},
  controller: {type: 'admin.acl.editor'},
  bind: {
    title: '{"mod-admin-acls:ADD_ACL":translate}',
  },
  layout: 'fit',
  closable: true,
  height: 530,
  width: 300,
  listeners: {
    initialize: 'onInitialize'
  },
  items: [{
    xtype: 'formpanel',
    reference: 'aclForm',
    layout: 'vbox',
    padding: 10,
    items: [{
      xtype: 'textfield',
      name: 'model',
      required: true,
      bind: {
        label: '{"mod-admin-acls:ACL_MODEL":translate}',
        value: '{acl.model}'
      }
    }, {
      xtype: 'textfield',
      name: 'property',
      required: true,
      bind: {
        label: '{"mod-admin-acls:ACL_PROPERTY":translate}',
        value: '{acl.property}'
      }
    }, {
      xtype: 'selectfield',
      name: 'accessType',
      required: true,
      bind: {
        label: '{"mod-admin-acls:ACL_ACCESSTYPE":translate}',
        value: '{acl.accessType}'
      },
      options: [{
        text: '*',
        value: '*'
      }, {
        text: 'EXECUTE',
        value: 'EXECUTE'
      }, {
        text: 'READ',
        value: 'READ'
      }, {
        text: 'WRITE',
        value: 'WRITE'
      }]
    }, {
      xtype: 'selectfield',
      name: 'permission',
      required: true,
      bind: {
        label: '{"mod-admin-acls:ACL_PERMISSION":translate}',
        value: '{acl.permission}'
      },
      options: [{
        text: 'DENY',
        value: 'DENY'
      }, {
        text: 'ALLOW',
        value: 'ALLOW'
      }]
    }, {
      xtype: 'selectfield',
      name: 'principalType',
      required: true,
      bind: {
        label: '{"mod-admin-acls:ACL_PRINCIPALTYPE":translate}',
        value: '{acl.principalType}'
      },
      options: [{
        text: 'USER',
        value: 'USER'
      }, {
        text: 'APP',
        value: 'APP'
      }, {
        text: 'ROLE',
        value: 'ROLE'
      }]
    }, {
      xtype: 'textfield',
      name: 'principalId',
      required: true,
      bind: {
        label: '{"mod-admin-acls:ACL_PRINCIPALID":translate}',
        value: '{acl.principalId}'
      }
    }]
  }, {
    xtype: 'toolbar',
    docked: 'bottom',
    items: [{
      xtype: 'button',
      ui: 'rounded raised',
      text: 'Ok',
      bind: {
        disabled: '{!acl.dirty}'
      },
      handler: 'okHandler'
    }, {
      xtype: 'spacer'
    }, {
      xtype: 'button',
      ui: 'rounded raised',
      text: 'Cancel',
      bind: {
        disabled: '{!acl.dirty}'
      },
      handler: 'cancelHandler'
    }]
  }]
});
