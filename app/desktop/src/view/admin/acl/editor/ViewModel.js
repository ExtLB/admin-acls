Ext.define('Client.view.admin.acl.EditorViewModel', {
  extend: 'Ext.app.ViewModel',
  alias: 'viewmodel.admin.acl.editor',

  data: {
    action: "add",
    acl: {}
  }
});
