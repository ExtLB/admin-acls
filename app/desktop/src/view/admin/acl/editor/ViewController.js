Ext.define('Client.view.admin.acl.EditorController', {
  extend: 'Ext.app.ViewController',
  alias: 'controller.admin.acl.editor',

  okHandler: function(btn) {
    let me = this;
    let view = me.getView();
    let vm = view.getViewModel();
    if (view.handler) {
      view.handler(vm.get('acl'));
    }
    view.destroy();
  },
  cancelHandler: function(btn) {
    let me = this;
    let view = me.getView();
    let vm = view.getViewModel();
    vm.get('acl').reject();
    this.getView().destroy();
  },

  onInitialize: function (editor) {
    let me = this;
    let view = me.getView();
    let vm = view.getViewModel();
    if (!_.isUndefined(view.acl)) {
      vm.set('acl', view.acl);
      vm.set('action', 'edit');
      view.setBind({
        title: '{"saa:EDIT_ACL":translate}',
      });
    } else {
      vm.set('acl', {
        name: '',
        description: '',
      });
    }
  }
});
