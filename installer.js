let config = require('./extlb');
module.exports = function (configs) {
  if (configs['model-config.json']._meta.sources.indexOf(`../node_modules/${config.name}/common/models`) === -1) {
    configs['model-config.json']._meta.sources.push(`../node_modules/${config.name}/common/models`);
  }
  if (configs['model-config.json']._meta.sources.indexOf(`../node_modules/${config.name}/server/models`) === -1) {
    configs['model-config.json']._meta.sources.push(`../node_modules/${config.name}/server/models`);
  }
  if (configs['model-config.json']._meta.mixins.indexOf(`../node_modules/${config.name}/common/mixins`) === -1) {
    configs['model-config.json']._meta.mixins.push(`../node_modules/${config.name}/common/mixins`);
  }
  if (configs['model-config.json']._meta.mixins.indexOf(`../node_modules/${config.name}/server/mixins`) === -1) {
    configs['model-config.json']._meta.mixins.push(`../node_modules/${config.name}/server/mixins`);
  }
  return configs;
};
